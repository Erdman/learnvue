import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/users',
      name: 'Users',
      component: () => import('@/views/users/Users.vue'),
    },
    {
      path: '/users/:userId',
      name: 'UserEdit',
      component: () => import('@/views/users/Edit.vue'),
    },
    {
      path: '/add',
      name: 'UserAdd',
      component: () => import('@/views/users/Add.vue'),
    },
    {
      path: '/contacts',
      name: 'Contacts',
      component: () => import('@/views/users/Contacts.vue'),
    },
  ],
});
