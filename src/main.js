import 'bulma/css/bulma.css';
import '@fortawesome/fontawesome-free/js/all';

import Vue from 'vue';
import Vuex from 'vuex';

import VueConfig from 'vue-configuration';
import Notifications from 'vue-notification';

import NProgress from 'nprogress';
import 'nprogress/nprogress.css';

import VeeValidate from 'vee-validate';
import veeValidateDictionary from './modules/veeValidate/veeValidate-dictionary';

import App from './App.vue';
import router from './router';
import appConfig from './appConfig';


Vue.config.productionTip = false;

Vue.use(Vuex);

Vue.use(VueConfig, {
  config: appConfig,
});

NProgress.configure({ showSpinner: false });
Vue.mixin({
  created() {
    this.$nprogress = NProgress;
  },
});

Vue.use(Notifications);
Vue.use(VeeValidate, {
  locale: 'ru',
  dictionary: veeValidateDictionary,
});


new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
