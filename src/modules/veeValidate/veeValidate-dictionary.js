export default {
  ru: {
    messages: {
      required: 'Обязательно для заполнения',
      email: 'Неправильный e-mail',
      url: 'Неправильный url-адрес',
      decimal: 'Неправильное число',
    },
  },
};
